package com.sainsbury.service.productDetailPage;

import com.sainsbury.models.exceptions.CouldNotConnectException;
import com.sainsbury.models.exceptions.CouldNotLoadMainDocumentException;
import com.sainsbury.service.HtmlFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;


public class ProductPageTests {

    private SainsburyProductPageScrapperImpl productPageScrapper;

    @Before
    public void before() {
        productPageScrapper = new SainsburyProductPageScrapperImpl();
    }

    @Test(expected = CouldNotConnectException.class)
    public void givenInvalidDocumentScrapperShouldThrowException() throws CouldNotConnectException, CouldNotLoadMainDocumentException {
        productPageScrapper.execute(new Document(""));
    }

    @Test
    public void givenValidProductShouldScrapeDescription() {
        String statementDescription = productPageScrapper.scrapeDescription(Jsoup.parse(HtmlFactory.getProductDetailHtmlFragment()));
        Assert.assertNotNull(statementDescription);
        Assert.assertThat(statementDescription, equalTo("by Sainsbury's strawberries"));
    }


    @Test
    public void givenValidProductShouldScrapeKCalPer100g() {
        Integer kCalPer100g = productPageScrapper.scrapeKCalPer100g(Jsoup.parse(HtmlFactory.getProductDetailHtmlFragment()));
        Assert.assertNotNull(kCalPer100g);
        Assert.assertThat(kCalPer100g, equalTo(33));
    }


}
