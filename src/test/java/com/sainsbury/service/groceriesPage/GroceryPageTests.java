package com.sainsbury.service.groceriesPage;

import com.sainsbury.models.Product;
import com.sainsbury.models.ScrapperOutput;
import com.sainsbury.models.exceptions.CouldNotLoadMainDocumentException;
import com.sainsbury.service.HtmlFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;


public class GroceryPageTests {

    private SainsburyGroceryPageScrapperImpl groceryPageScrapper;
    private Element product;

    @Before
    public void before() {
        groceryPageScrapper = new SainsburyGroceryPageScrapperImpl();
        product = Jsoup.parse(HtmlFactory.getShelfProductHtmlFragment());
    }

    @Test(expected = CouldNotLoadMainDocumentException.class)
    public void givenInvalidDocumentScrapperShouldThrowException() throws CouldNotLoadMainDocumentException, MalformedURLException, InterruptedException {
        groceryPageScrapper.execute(new URL("https://localhost:9999"));
    }

    @Test
    public void givenValidDocumentShouldReturnProductList() {
        Elements elements = groceryPageScrapper.scrapeProductList(Jsoup.parse(HtmlFactory.getProductListHtmlFragment()));
        Assert.assertNotNull("Scrapper should be able to return elements", elements);
        Assert.assertThat("Scrapper should be able to return a list with size 2", elements.size(), equalTo(2));
    }

    @Test
    public void givenValidDocumentShouldReturnTitle() {
        String title = groceryPageScrapper.scrapeTitle(product);
        Assert.assertNotNull("Scrapper should be able to return title", title);
        Assert.assertThat(title, equalTo("Sainsbury's Strawberries 400g"));
    }

    @Test
    public void givenValidDocumentShouldReturnUnitPrice() {
        BigDecimal unitPrice = groceryPageScrapper.scrapeUnitPrice(product);
        Assert.assertNotNull("Scrapper should be able to return unit price", unitPrice);
        Assert.assertThat(unitPrice, equalTo(BigDecimal.valueOf(1.75)));
    }

    @Test
    public void givenProductListShouldReturnOutput() {
        ScrapperOutput output = groceryPageScrapper.buildOutput(Arrays.asList(
                new Product("title", 10, BigDecimal.TEN, "description"),
                new Product("title", 15, BigDecimal.TEN, "description")
        ));

        Assert.assertNotNull("Scrapper should be able to produce output with a valid product list", output);

        Assert.assertNotNull("Scrapper should be able to produce output with a valid product list", output.getResults());

        Assert.assertThat("Scrapper should return a result list with size 2", output.getResults().size(), equalTo(2));

        Assert.assertNotNull("Scrapper should calculate the Total", output.getTotal());

        Assert.assertThat(output.getTotal().getGross(), equalTo(BigDecimal.valueOf(20)));

        Assert.assertThat(output.getTotal().getVat(), equalTo(BigDecimal.valueOf(4).setScale(2)));

    }

}
