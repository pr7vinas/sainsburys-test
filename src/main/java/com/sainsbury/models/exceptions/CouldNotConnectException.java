package com.sainsbury.models.exceptions;

import java.net.MalformedURLException;

public class CouldNotConnectException extends Exception {
    public CouldNotConnectException(MalformedURLException e) {
        super(e);
    }
}
