package com.sainsbury.models.exceptions;

import java.io.IOException;

public class CouldNotLoadMainDocumentException extends Exception {
    public CouldNotLoadMainDocumentException(IOException e) {
        super(e);
    }
}
