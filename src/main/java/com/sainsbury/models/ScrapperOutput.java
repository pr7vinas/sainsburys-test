package com.sainsbury.models;

import java.util.List;

public class ScrapperOutput {

    private List<Product> results;

    private Total total;

    public ScrapperOutput(List<Product> products, Total total) {
        this.results = products;
        this.total = total;
    }

    public List<Product> getResults() {
        return results;
    }

    public void setResults(List<Product> results) {
        this.results = results;
    }

    public Total getTotal() {
        return total;
    }

    public void setTotal(Total total) {
        this.total = total;
    }
}
