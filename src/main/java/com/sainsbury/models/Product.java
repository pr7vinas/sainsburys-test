package com.sainsbury.models;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class Product {

    public Product() {
    }

    public Product(String title, Integer kCalPer100g, BigDecimal unitPrice, String description) {
        this.title = title;
        this.kCalPer100g = kCalPer100g;
        this.unitPrice = unitPrice;
        this.description = description;
    }

    private String title;

    @SerializedName("kcal_per_100g")
    private Integer kCalPer100g;

    @SerializedName("unit_price")
    private BigDecimal unitPrice;

    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getkCalPer100g() {
        return kCalPer100g;
    }

    public void setkCalPer100g(Integer kCalPer100g) {
        this.kCalPer100g = kCalPer100g;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
