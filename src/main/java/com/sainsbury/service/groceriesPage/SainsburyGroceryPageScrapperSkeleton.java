package com.sainsbury.service.groceriesPage;

import com.sainsbury.models.Product;
import com.sainsbury.models.ProductPageDetail;
import com.sainsbury.models.ScrapperOutput;
import com.sainsbury.models.exceptions.CouldNotLoadMainDocumentException;
import com.sainsbury.service.AbstractJsoupScrapper;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;

public abstract class SainsburyGroceryPageScrapperSkeleton extends AbstractJsoupScrapper<URL, ScrapperOutput> {

    @Override
    public final ScrapperOutput execute(URL url) throws InterruptedException, CouldNotLoadMainDocumentException {
        final Connection connection = this.getConnection(url);
        final Document document = this.getHtmlPage(connection);

        final Queue<Product> products = new ConcurrentLinkedQueue<>();

        final ExecutorService executor = Executors.newWorkStealingPool();

        List<Callable<Boolean>> callableList = new ArrayList<>();

        for (Element productElement : this.scrapeProductList(document)) {
            callableList.add(() -> {
                Product product = new Product();

                ProductPageDetail productPageDetail = this.scrapeProductPageDetail(productElement);

                product.setTitle(this.scrapeTitle(productElement));
                product.setDescription(productPageDetail.getDescription());
                product.setkCalPer100g(productPageDetail.getkCalPer100g());
                product.setUnitPrice(this.scrapeUnitPrice(productElement));

                products.add(product);

                return true;
            });

        }

        executor.invokeAll(callableList);

        return this.buildOutput(Arrays.asList(products.toArray(new Product[0])));

    }

    protected abstract Elements scrapeProductList(Document document);

    protected abstract String scrapeTitle(Element product);

    protected abstract BigDecimal scrapeUnitPrice(Element product);

    protected abstract ProductPageDetail scrapeProductPageDetail(Element product);

    protected abstract ScrapperOutput buildOutput(List<Product> products);

}
