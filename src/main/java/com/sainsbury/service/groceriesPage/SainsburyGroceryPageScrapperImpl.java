package com.sainsbury.service.groceriesPage;

import com.google.common.annotations.VisibleForTesting;
import com.sainsbury.models.Product;
import com.sainsbury.models.ProductPageDetail;
import com.sainsbury.models.ScrapperOutput;
import com.sainsbury.models.Total;
import com.sainsbury.models.exceptions.CouldNotConnectException;
import com.sainsbury.models.exceptions.CouldNotLoadMainDocumentException;
import com.sainsbury.service.productDetailPage.SainsburyProductPageScrapperImpl;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public final class SainsburyGroceryPageScrapperImpl extends SainsburyGroceryPageScrapperSkeleton {

    private static final double VAT_PERCENTAGE = 0.2D;
    private static final int SCALE = 2;

    @Override
    @VisibleForTesting
    protected Elements scrapeProductList(Document document) {
        return document.select("li.gridItem div.product");
    }

    @Override
    @VisibleForTesting
    protected String scrapeTitle(Element product) {
        return product.select("div.productInfo div.productNameAndPromotions h3 a").text();
    }

    @Override
    @VisibleForTesting
    protected BigDecimal scrapeUnitPrice(Element product) {
        Element priceElement = product.select("div.addToTrolleytabBox  p.pricePerUnit").first();
        if (priceElement == null) {
            return null;
        } else {
            String price = priceElement.text()
                    .replace("£", "")
                    .replace("/unit", "");

            return new BigDecimal(price).setScale(2, RoundingMode.CEILING);
        }
    }

    @Override
    protected ProductPageDetail scrapeProductPageDetail(Element product) {
        SainsburyProductPageScrapperImpl productPageScraper = new SainsburyProductPageScrapperImpl();
        try {
            return productPageScraper.execute(product);
        } catch (CouldNotConnectException | CouldNotLoadMainDocumentException e) {
            return new ProductPageDetail();
        }
    }


    @Override
    @VisibleForTesting
    protected ScrapperOutput buildOutput(List<Product> products) {
        AtomicReference<BigDecimal> gross = new AtomicReference<>(BigDecimal.ZERO);
        products.forEach(product -> gross.updateAndGet(g -> g.add(product.getUnitPrice())));

        BigDecimal decimalGross = gross.get();
        BigDecimal vat = decimalGross.multiply(BigDecimal.valueOf(VAT_PERCENTAGE)).setScale(SCALE, RoundingMode.CEILING);

        return new ScrapperOutput(products, new Total(decimalGross, vat));
    }
}
