package com.sainsbury.service.productDetailPage;

import com.google.common.annotations.VisibleForTesting;
import com.sainsbury.models.ProductPageDetail;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class SainsburyProductPageScrapperImpl extends SainsburyProductPageScrapperSkeleton {

    @Override
    @VisibleForTesting
    protected Integer scrapeKCalPer100g(Document document) {
        Element element = document.select("#information > productcontent > htmlcontent > div:nth-child(4) > div > div > table > tbody > tr:nth-child(2) > td:nth-child(1)").first();
        if (element == null) {
            return null;
        }

        return Integer.valueOf(element.text().replace("kcal", ""));
    }

    @Override
    @VisibleForTesting
    protected String scrapeDescription(Document document) {
        Element description = getMemoDescription(document);
        if (description == null) {
            description = getStatementDescription(document);
            if (description == null) {
                description = getDefaultDescription(document);
                if (description == null) {
                    return null;
                }
            }
        }
        return description.text();
    }

    private Element getMemoDescription(Document document) {
        Element element = document.select("#mainPart > div > div.memo").first();
        if (element == null) {
            element = document.select("#mainPart > div:nth-child(1) > div.memo > p").first();
        }
        return element;
    }

    private Element getDefaultDescription(Document document) {
        return document.select("#information > productcontent > htmlcontent > div:nth-child(2n) > p:nth-child(1n)").first();
    }

    private Element getStatementDescription(Document document) {
        return document.select("#mainPart > div > div > p:nth-child(2)").first();
    }

    @Override
    protected ProductPageDetail buildOutput(String description, Integer kCalPer100g) {
        ProductPageDetail productPageDetail = new ProductPageDetail();
        productPageDetail.setkCalPer100g(kCalPer100g);
        productPageDetail.setDescription(description);
        return productPageDetail;

    }

}