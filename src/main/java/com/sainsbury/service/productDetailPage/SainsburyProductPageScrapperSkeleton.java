package com.sainsbury.service.productDetailPage;

import com.sainsbury.models.ProductPageDetail;
import com.sainsbury.models.exceptions.CouldNotConnectException;
import com.sainsbury.models.exceptions.CouldNotLoadMainDocumentException;
import com.sainsbury.service.AbstractJsoupScrapper;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

public abstract class SainsburyProductPageScrapperSkeleton extends AbstractJsoupScrapper<Element, ProductPageDetail> {

    private static final Logger logger = LoggerFactory.getLogger(SainsburyProductPageScrapperSkeleton.class);


    @Override
    public ProductPageDetail execute(Element product) throws CouldNotLoadMainDocumentException, CouldNotConnectException {
        final Connection connection = this.getConnection(product);
        final Document document = this.getHtmlPage(connection);

        String description = this.scrapeDescription(document);
        Integer kCalPer100g = this.scrapeKCalPer100g(document);

        return this.buildOutput(description, kCalPer100g);
    }

    protected abstract ProductPageDetail buildOutput(String description, Integer kCalPer100g);

    protected abstract Integer scrapeKCalPer100g(Document document);

    protected abstract String scrapeDescription(Document document);

    private Connection getConnection(Element product) throws CouldNotConnectException {
        try {
            return super.getConnection(new URL(product.select("div.productNameAndPromotions h3 a").attr("abs:href")));
        } catch (MalformedURLException e) {
            logger.error("Could not get connection for product detail page due to", e);
            throw new CouldNotConnectException(e);
        }
    }
}
