package com.sainsbury.service;


import com.sainsbury.models.exceptions.CouldNotLoadMainDocumentException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class AbstractJsoupScrapper<INPUT, OUTPUT> implements Scrapper<INPUT, OUTPUT> {

    private final static Logger LOG = LoggerFactory.getLogger(AbstractJsoupScrapper.class);

    protected Connection getConnection(URL url) {
        return Jsoup.connect(url.toString());
    }

    protected Document getHtmlPage(Connection connection) throws CouldNotLoadMainDocumentException {
        try {
            return connection.get();
        } catch (IOException e) {
            LOG.error("Could not get html page", e);
            throw new CouldNotLoadMainDocumentException(e);
        }
    }

}
