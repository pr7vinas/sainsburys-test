package com.sainsbury.service;

import com.sainsbury.models.exceptions.CouldNotConnectException;
import com.sainsbury.models.exceptions.CouldNotLoadMainDocumentException;

@FunctionalInterface
public interface Scrapper<INPUT, OUTPUT> {

    OUTPUT execute(INPUT input) throws CouldNotLoadMainDocumentException, CouldNotConnectException, InterruptedException;

}
