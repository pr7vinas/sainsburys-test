package com.sainsbury;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sainsbury.models.ScrapperOutput;
import com.sainsbury.models.exceptions.CouldNotConnectException;
import com.sainsbury.models.exceptions.CouldNotLoadMainDocumentException;
import com.sainsbury.service.groceriesPage.SainsburyGroceryPageScrapperImpl;
import com.sainsbury.service.Scrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

import static java.lang.System.*;

public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {

        Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();

        Scrapper<URL, ScrapperOutput> scrapper = new SainsburyGroceryPageScrapperImpl();
        ScrapperOutput output = null;

        try {
            output = scrapper.execute(getUrl());
        } catch (MalformedURLException | CouldNotConnectException | InterruptedException | CouldNotLoadMainDocumentException e) {
            logger.error("Application failed due to", e);
        }

        out.println(gson.toJson(output));

    }

    private static URL getUrl() throws MalformedURLException {
        return new URL("https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html");
    }
}
