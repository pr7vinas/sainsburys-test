# Sainsbury Scraper Challenge
This is a [challange](https://jsainsburyplc.github.io/serverside-test/) for [Sainsbury's](https://www.sainsburys.co.uk/) recruitment process 


## How to run on local

1. `mvn clean package` 

2. `java -jar target/App.jar`

---

### Design and Architecture of the App

1. `Skeleton Design Partner` - used to clearly define the set of instructions necessary to be performed for each scrapper (Groceries Scrapper and Product Detail Scrapper)
2. `Generics` - a combination of _interfaces_ and _abstract_ classes were used in this project to allow further extensibility and modularity.
3. `Multithreading` - a good portion of the scraping is done in parallel, which for the range of data present, seemed like a good opportunity to apply such concept.

---

### Tools

1. `Jsoup` - html scraping
2. `Gson` - des/serialization
3. `Junit` - testing
4. `Hamcrest` - testing
5. `Guava` - testing
6. `Sl4j` - logging
7. `Onejar` - uberjar

---

### Author

Vinicius Domingues Alvarenga

pr7vinas@gmail.com